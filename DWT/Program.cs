﻿namespace Parsec.DWT
{
    using System;
    using System.IO;
    using System.Text;

    public class Program
    {
        static void Main(string[] args)
        {
            var toWin = string.Compare(args[0], "w", true) == 0;
            var sourceFile = args[1];
            var destFile = Path.ChangeExtension(sourceFile, toWin ? "win" : "dos");

            var text = File.ReadAllText(sourceFile, toWin ? Encoding.GetEncoding(866) : Encoding.Default);
            File.WriteAllText(destFile, text, toWin ? Encoding.Default : Encoding.GetEncoding(866));

            Console.WriteLine($"{destFile} done ({text.Length} characters)");
        }
    }
}
